package com.example.notes_signup.model;

public class FirebaseModel {
    private String title;
    private String content;
    private String imageUrl;

    private String uniqueFilename;
    public FirebaseModel(String title, String content, String imageUrl, String uniqueFilename){
        this.title = title;
        this.content = content;
        this.imageUrl = imageUrl;
        this.uniqueFilename = uniqueFilename;
    }

    public String getUniqueFilename() {
        return uniqueFilename;
    }

    public void setUniqueFilename(String uniqueFilename) {
        this.uniqueFilename = uniqueFilename;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public FirebaseModel(){}
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}


