package com.example.notes_signup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    private TextView loginoption2;
    private EditText email;
    private EditText password;
    private MaterialButton loginbtn;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Objects.requireNonNull(getSupportActionBar()).hide();
        mountView();
        this.firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser firebaseUser = this.firebaseAuth.getCurrentUser();
        if (firebaseUser != null) {
            finish();
            startActivity(new Intent(getApplicationContext(), NotesActivity.class));
        }
    }

    private void mountView() {
        this.loginoption2 = findViewById(R.id.loginoption2);
        this.email = findViewById(R.id.email);
        this.password = findViewById(R.id.password);
        this.loginbtn = findViewById(R.id.loginbtn);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EditText Email2 = this.email;
        EditText password = this.password;
        FirebaseAuth firebaseAuth = this.firebaseAuth;
        this.loginbtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String Email = Email2.getText().toString().trim();
                String passWord = password.getText().toString().trim();
                if (Email.isEmpty() || passWord.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Field email dan password tidak boleh kosong", Toast.LENGTH_LONG).show();
                } else {
                    Log.d("email", Email);
                    Log.d("password", passWord);
                    firebaseAuth.signInWithEmailAndPassword(Email, passWord).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Log.d("is task successfull",task.isSuccessful() + "");
                            if (task.isSuccessful()) {
                                checkMailVerification();
                            } else {
                                Toast.makeText(getApplicationContext(), "Akun tidak ditemukan atau password anda salah", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                }
            }
        });
        this.loginoption2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SignupActivity.class);
                startActivity(intent);
            }
        });
    }

    private void checkMailVerification() {
        FirebaseUser firebaseUser = this.firebaseAuth.getCurrentUser();
        assert firebaseUser != null;
        if (firebaseUser.isEmailVerified()) {
            Toast.makeText(getApplicationContext(), "User berhasil login", Toast.LENGTH_LONG).show();
            finish();
            startActivity(new Intent(MainActivity.this, NotesActivity.class));
        } else {
            Toast.makeText(getApplicationContext(), "Mohon verifikasi email anda terlebih dahulu", Toast.LENGTH_LONG).show();
            firebaseAuth.signOut();
        }
    }
}
