package com.example.notes_signup;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

public class DetailNoteActivity extends AppCompatActivity {
    FloatingActionButton gotoEdit;
    TextView textViewTitle;
    TextView textViewContent;
    ImageView imageViewContent;
    String imageUrl;
    CardView cardViewContentImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_note);
        Objects.requireNonNull(getSupportActionBar()).hide();
        this.gotoEdit = findViewById(R.id.gotoedit);
        this.textViewTitle = findViewById(R.id.tv_note_title);
        this.textViewContent = findViewById(R.id.tv_note_content);
        this.imageViewContent = findViewById(R.id.image_content);
        this.cardViewContentImage = findViewById(R.id.image_content_cardview);
        Intent data = getIntent();
        gotoEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailNoteActivity.this, EditActivity.class);
                intent.putExtra("title", data.getStringExtra("title"));
                intent.putExtra("content", data.getStringExtra("content"));
                intent.putExtra("docId", data.getStringExtra("docId"));
                intent.putExtra("imageUrl", data.getStringExtra("imageUrl"));
                intent.putExtra("uniqueFilename", data.getStringExtra("uniqueFilename"));
                startActivity(intent);
            }
        });
        this.textViewTitle.setText(data.getStringExtra("title"));
        this.textViewContent.setText(data.getStringExtra("content"));
        this.imageUrl = data.getStringExtra("imageUrl");

        if (imageUrl == null || imageUrl.isEmpty()) {
            this.cardViewContentImage.setVisibility(View.INVISIBLE);
            this.cardViewContentImage.setVisibility(View.GONE);
        } else {
            Glide.with(this).load(imageUrl).centerCrop().into(imageViewContent);
        }
        this.imageViewContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadImage(imageUrl);
            }
        });
    }

    private void downloadImage(String imageUrl) {
        // Use a library like Picasso or DownloadManager to download the image
        // For simplicity, let's assume you have a method downloadImageByUrl() for downloading

        // Example using Picasso
        Glide.with(this)
                .asBitmap()
                .load(imageUrl)
                .into(new CustomTarget<Bitmap>() {

                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        // Fix orientation if needed
                        int orientation = getOrientation(imageUrl);
                        if (orientation != 0) {
                            Matrix matrix = new Matrix();
                            matrix.postRotate(orientation);
                            resource = Bitmap.createBitmap(resource, 0, 0, resource.getWidth(), resource.getHeight(), matrix, true);
                        }

                        // Save the downloaded and oriented image to a file
                        saveImageToGallery(resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                        // Handle resource cleared
                    }
                });

    }

    private int getOrientation(String imagePath) {
        try {
            ExifInterface exifInterface = new ExifInterface(imagePath);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return 90;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return 180;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return 270;
                default:
                    return 0;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private void saveImageToGallery(Bitmap bitmap) {
        // Save the bitmap to a file
        // For simplicity, let's assume you have a method saveBitmapToFile() for saving the bitmap to a file

        File imageFile = saveBitmapToFile(bitmap);

        // Update the gallery
        MediaScannerConnection.scanFile(this,
                new String[]{imageFile.getAbsolutePath()},
                null,
                (path, uri) -> {
                    // Image is saved and visible in the gallery
                    Toast.makeText(getApplicationContext(), "Image saved to gallery", Toast.LENGTH_SHORT).show();
                    openImageInGallery(uri);
                });
    }

    private void openImageInGallery(Uri imageUri) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(imageUri, "image/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        // Verify that the intent will resolve to an activity
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(this, "No gallery app found", Toast.LENGTH_SHORT).show();
        }
    }

    private File saveBitmapToFile(Bitmap bitmap) {
        // Get the directory to save images
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "YourDirectoryName");

        // Create the directory if it doesn't exist
        if (!directory.exists()) {
            directory.mkdirs();
        }

        // Create a file name for the image
        String fileName = "image_" + System.currentTimeMillis() + ".jpg";

        // Create the file in the specified directory
        File imageFile = new File(directory, fileName);

        try {
            // Create a FileOutputStream to write the bitmap data to the file
            FileOutputStream outputStream = new FileOutputStream(imageFile);

            // Compress the bitmap to JPEG format with quality 100 (you can adjust this)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

            // Close the FileOutputStream
            outputStream.close();

            return imageFile;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
