package com.example.notes_signup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Objects;

public class SignupActivity extends AppCompatActivity {
    private TextView signupoption2;
    private MaterialButton signupbtn;
    private EditText email;
    private EditText password;
    private EditText confirm;
    private FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mountView();
        Objects.requireNonNull(getSupportActionBar()).hide();
        this.firebaseAuth = FirebaseAuth.getInstance();
    }
    private void mountView(){
        this.signupoption2 = findViewById(R.id.signupoption2);
        this.signupbtn = findViewById(R.id.signupbtn);
        this.email = findViewById(R.id.email);
        this.password = findViewById(R.id.password);
        this.confirm = findViewById(R.id.confirm);
    }

    private void sendEmailVerification(){
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        if(firebaseUser != null) {
            firebaseUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Toast.makeText(getApplicationContext(),"Email verifikasi telah dikirim",Toast.LENGTH_LONG).show();
                    firebaseAuth.signOut();
                    finish();
                    startActivity(new Intent(SignupActivity.this,MainActivity.class));
                }
            });
        }else{
            Toast.makeText(getApplicationContext(),"Gagal untuk mengirim email verifikasi",Toast.LENGTH_LONG).show();
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        EditText email = this.email;
        EditText password = this.password;
        EditText confirm = this.confirm;
//        Signup user action
        this.signupbtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String Email = email.getText().toString().trim();
                String passWord = password.getText().toString().trim();
                String Confirm = confirm.getText().toString().trim();
                if(Email.isEmpty() || passWord.isEmpty() || Confirm.isEmpty()){
                    Toast.makeText(SignupActivity.this,"Semua field harus diisi",Toast.LENGTH_LONG).show();
                }else if(!passWord.equals(Confirm)){
                    Toast.makeText(SignupActivity.this,"Password dan confirm password tidak sesuai",Toast.LENGTH_LONG).show();
                }else{
                    firebaseAuth.createUserWithEmailAndPassword(Email,passWord).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(getApplicationContext(),"Register berhasil",Toast.LENGTH_LONG).show();
                                sendEmailVerification();
                            }else{
                                Toast.makeText(getApplicationContext(),"Gagal mendaftar",Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });
//        Move to Signing user action
        this.signupoption2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
