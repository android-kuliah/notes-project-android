package com.example.notes_signup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class CreateNoteActivity extends AppCompatActivity {

    private EditText createTitleNote;
    private EditText createContentNote;
    private FloatingActionButton saveNoteButton;
    private ImageView imageView;
    private Uri imageUri;
    ProgressBar progressBar;
    AppCompatImageButton appCompatImageButton;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    FirebaseFirestore firebaseFirestore;
    StorageReference storageReference;
    ActivityResultLauncher<Intent> actionOpenApp;

    ImageButton addnewImage;

    LinearLayout LlImageEditor;
    ImageButton deleteAttachedImg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Create new note");
        this.createTitleNote = findViewById(R.id.et_note_title);
        this.createContentNote = findViewById(R.id.et_note_content);
        this.saveNoteButton = findViewById(R.id.savenotfab);
        this.appCompatImageButton = findViewById(R.id.upload_btn);
        this.imageView = findViewById(R.id.image_up);
        this.addnewImage = findViewById(R.id.add_new_image);
        this.deleteAttachedImg = findViewById(R.id.delete_attached_img);
        this.LlImageEditor = findViewById(R.id.ll_image_editor);

        this.firebaseAuth = FirebaseAuth.getInstance();
        this.firebaseFirestore = FirebaseFirestore.getInstance();
        this.firebaseUser = this.firebaseAuth.getCurrentUser();
        this.storageReference = FirebaseStorage.getInstance().getReference();

        refreshRender();

        this.saveNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = createTitleNote.getText().toString();
                String content = createContentNote.getText().toString();
                if (title.isEmpty() || content.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Semua field diperlukan", Toast.LENGTH_SHORT).show();
                } else {
                    uploadToFirebase(title, content);
                }
            }
        });
        this.actionOpenApp = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult o) {
                if (o.getResultCode() == Activity.RESULT_OK) {
                    Log.d("image taken", "taken");
                    Intent data = o.getData();
                    imageUri = data.getData();
                    if (imageUri != null) {
                        refreshRender();
                    } else {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        Log.d("bitmap",photo == null ? "it is null" : "its not null");
                        imageUri = getImageUriFromPath(saveBitmapToGallery(photo,"NotesKu_Photo"));
                        refreshRender();
                    }

                } else {
                    Toast.makeText(CreateNoteActivity.this, "No image selected", Toast.LENGTH_SHORT).show();
                }
            }
        });
        this.appCompatImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchAppPicker();
            }
        });

        this.addnewImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchAppPicker();
            }
        });

        this.deleteAttachedImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageUri = null;
                refreshRender();
            }
        });
    }

    public String saveBitmapToGallery(Bitmap bitmap, String title) {
        String savedImagePath = null;

        // Create a directory if it doesn't exist
        File imagesFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "NotesKu");
        if (!imagesFolder.exists()) {
            imagesFolder.mkdirs();
        }

        // Create a unique file name
        String imageFileName = "IMG_" + title + "_" + System.currentTimeMillis() + ".jpg";

        // Save the bitmap to the file
        File imageFile = new File(imagesFolder, imageFileName);
        savedImagePath = imageFile.getAbsolutePath();
        try {
            FileOutputStream fos = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Notify the system that a new file has been created
        MediaScannerConnection.scanFile(this, new String[]{imageFile.getAbsolutePath()}, null, null);

        return savedImagePath;
    }

    public Uri getImageUriFromPath(String imagePath) {
        return Uri.fromFile(new File(imagePath));
    }


    @SuppressLint("QueryPermissionsNeeded")
    private void launchAppPicker() {
        Intent photoPicker = new Intent(Intent.ACTION_GET_CONTENT);
        photoPicker.setType("image/*");
        photoPicker.putExtra("origin", "media");
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra("origin", "camera");
        Intent chooserIntent = Intent.createChooser(photoPicker, "Choose an option");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{cameraIntent});
        actionOpenApp.launch(chooserIntent);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    private void refreshRender() {
        if (this.imageUri == null) {
            appCompatImageButton.setVisibility(View.VISIBLE);
            imageView.setImageURI(null);
            imageView.setVisibility(View.INVISIBLE);
            LlImageEditor.setVisibility(View.GONE);
        } else {
            imageView.setImageURI(null);
            imageView.setImageURI(this.imageUri);
            imageView.setVisibility(View.VISIBLE);
            appCompatImageButton.setVisibility(View.INVISIBLE);
            LlImageEditor.setVisibility(View.VISIBLE);
        }
    }


    private void uploadNoteToFirebase(String imageUrl, String title, String content, String uniqueFilename) {
        DocumentReference documentReference = firebaseFirestore.collection("notes").document(firebaseUser.getUid()).collection("mynotes").document();
        Map<String, Object> note = new HashMap<>();
        note.put("title", title);
        note.put("content", content);
        note.put("imageUrl", imageUrl);
        note.put("uniqueFilename", uniqueFilename);
        documentReference.set(note).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(getApplicationContext(), "Note created successfully", Toast.LENGTH_SHORT).show();
                finish();
                startActivity(new Intent(getApplicationContext(), NotesActivity.class));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Failed to create note", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void uploadToFirebase(String title, String content) {
        if (this.imageUri == null) {
            uploadNoteToFirebase(null, title, content, null);
        } else {
            uploadImage(title, content);
        }
    }

    private void uploadImage(String title, String content) {
        String uniqueName = System.currentTimeMillis() + "." + getFileExtension(this.imageUri);
        final StorageReference imageRef = storageReference.child(uniqueName);
        imageRef.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Toast.makeText(CreateNoteActivity.this, "image uploaded", Toast.LENGTH_SHORT).show();
                        uploadNoteToFirebase(uri.toString(), title, content, uniqueName);
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(CreateNoteActivity.this, "Failed to upload image", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(contentResolver.getType(uri));
    }
}
