package com.example.notes_signup;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class EditActivity extends AppCompatActivity {

    EditText editTextTitleNote;
    EditText editTextTitleContent;
    FloatingActionButton floatingActionButtonSaveEdit;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    FirebaseFirestore firebaseFirestore;
    String docId;
    String imageUrl;
    String uniqueFilename;
    AppCompatImageButton addImageButton;
    ImageButton addNewImage;
    ImageButton deleteAttachedImage;

    LinearLayout layoutImageEditor;
    ImageView attachedImage;
    Uri imageUri;
    StorageReference storageReference;

    ActivityResultLauncher<Intent> actionOpenApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Editing note");
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        editTextTitleContent = findViewById(R.id.et_note_content_edit);
        editTextTitleNote = findViewById(R.id.et_note_title_edit);
        floatingActionButtonSaveEdit = findViewById(R.id.save_edit_note);
        addImageButton = findViewById(R.id.upload_btn_edit);
        addNewImage = findViewById(R.id.add_new_image_edit);
        deleteAttachedImage = findViewById(R.id.delete_attached_img_edit);
        layoutImageEditor = findViewById(R.id.ll_image_editor_edit);
        attachedImage = findViewById(R.id.image_up_edit);

        Intent data = getIntent();
        editTextTitleNote.setText(data.getStringExtra("title"));
        editTextTitleContent.setText(data.getStringExtra("content"));
        this.docId = data.getStringExtra("docId");
        this.imageUrl = data.getStringExtra("imageUrl");
        this.uniqueFilename = data.getStringExtra("uniqueFilename");

        handleRenderEditor();
        floatingActionButtonSaveEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newTitle = editTextTitleNote.getText().toString();
                String newContent = editTextTitleContent.getText().toString();
                if (newTitle.isEmpty() || newContent.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Field tidak boleh kosong", Toast.LENGTH_SHORT).show();
                } else {
                    if (imageUri == null && imageUrl != null) {
                        handleEdit(newContent, newTitle, data.getStringExtra("imageUrl"), data.getStringExtra("uniqueFilename"));
                    } else if (imageUri != null && imageUrl == null) {
                        handleEditWithNewImage(newContent, newTitle);
                    } else {
                        handleEditDeleteImage(newContent, newTitle);
                    }
                }
            }
        });
        this.actionOpenApp = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult o) {
                if (o.getResultCode() == Activity.RESULT_OK) {
                    Intent data = o.getData();
                    imageUri = data.getData();
                     if(imageUri == null) {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        imageUri = getImageUriFromPath(saveBitmapToGallery(photo, "NotesKu_Photo"));
                    }
                    imageUrl = null;
                    handleRenderEditor();
                } else {
                    Toast.makeText(EditActivity.this, "No image selected", Toast.LENGTH_SHORT).show();
                }
            }
        });
        this.addImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchPhotoPicker();
            }
        });

        this.addNewImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchPhotoPicker();
            }
        });

        this.deleteAttachedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageUri = null;
                imageUrl = null;
                handleRenderEditor();
            }
        });

    }
    public String saveBitmapToGallery(Bitmap bitmap, String title) {
        String savedImagePath = null;

        // Create a directory if it doesn't exist
        File imagesFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "YourAppFolder");
        if (!imagesFolder.exists()) {
            imagesFolder.mkdirs();
        }

        // Create a unique file name
        String imageFileName = "IMG_" + title + "_" + System.currentTimeMillis() + ".jpg";

        // Save the bitmap to the file
        File imageFile = new File(imagesFolder, imageFileName);
        savedImagePath = imageFile.getAbsolutePath();
        try {
            FileOutputStream fos = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Notify the system that a new file has been created
        MediaScannerConnection.scanFile(this, new String[]{imageFile.getAbsolutePath()}, null, null);

        return savedImagePath;
    }

    public Uri getImageUriFromPath(String imagePath) {
        return Uri.fromFile(new File(imagePath));
    }
    private void handleEditDeleteImage(String content, String title) {
        if (this.uniqueFilename != null) {
            StorageReference imgRef = this.storageReference.child(this.uniqueFilename);
            imgRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void unused) {
                    handleEdit(content, title, null, null);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    handleEdit(content, title, null, null);
                    Toast.makeText(EditActivity.this, "Gambar lama anda mungkin tidak terhapus", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    private void handleEditWithNewImage(String content, String title) {
        String uniqueName = System.currentTimeMillis() + "." + getFileExtension(this.imageUri);
        final StorageReference imageRef = storageReference.child(uniqueName);
        imageRef.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Toast.makeText(EditActivity.this, "image baru terupload", Toast.LENGTH_SHORT).show();
                        handleEditContentDoc(title, content, uri.toString(), uniqueName);
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(EditActivity.this, "Gagal mengupload gambar, note tidak diupdate", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void handleEditContentDoc(String title, String content, String imageUrl, String uniqueFilename) {
        DocumentReference documentReference = firebaseFirestore.collection("notes").document(firebaseUser.getUid()).collection("mynotes").document(Objects.requireNonNull(docId));
        Map<String, Object> map = new HashMap<>();
        map.put("title", title);
        map.put("content", content);
        map.put("imageUrl", imageUrl);
        map.put("uniqueFilename", uniqueFilename);

        String oldUniqueName = this.uniqueFilename;
        documentReference.set(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                if (oldUniqueName != null) {
                    StorageReference imgRef = storageReference.child(oldUniqueName);
                    imgRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            Toast.makeText(getApplicationContext(), "Note berhasil diupdate", Toast.LENGTH_SHORT).show();
                            finish();
                            startActivity(new Intent(getApplicationContext(), NotesActivity.class));
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(), "Note berhasil diupdate, gambar lama mungkin masih tersimpan", Toast.LENGTH_SHORT).show();
                            finish();
                            startActivity(new Intent(getApplicationContext(), NotesActivity.class));
                        }
                    });
                } else {
                    Toast.makeText(EditActivity.this, "Note berhasil diupdate", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), NotesActivity.class));
                }


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Note gagal diupdate", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    private void handleEdit(String content, String title, String imageUrl, String uniqueFilename) {
        DocumentReference documentReference = firebaseFirestore.collection("notes").document(firebaseUser.getUid()).collection("mynotes").document(Objects.requireNonNull(docId));
        Map<String, Object> map = new HashMap<>();
        map.put("title", title);
        map.put("content", content);
        map.put("imageUrl", imageUrl);
        map.put("uniqueFilename", uniqueFilename);
        documentReference.set(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(getApplicationContext(), "Note is updated", Toast.LENGTH_SHORT).show();
                finish();
                startActivity(new Intent(getApplicationContext(), NotesActivity.class));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Note failed for update", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void launchPhotoPicker() {
        Intent photoPicker = new Intent(Intent.ACTION_GET_CONTENT);
        photoPicker.setType("image/*");
        photoPicker.putExtra("origin", "media");
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra("origin", "camera");
        Intent chooserIntent = Intent.createChooser(photoPicker, "Choose an option");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{cameraIntent});
        actionOpenApp.launch(chooserIntent);
    }

    private void handleRenderEditor() {
        if (this.imageUrl == null && this.imageUri != null) {
            this.attachedImage.setImageURI(null);
            this.attachedImage.setImageURI(this.imageUri);
            this.attachedImage.setVisibility(View.VISIBLE);
            this.layoutImageEditor.setVisibility(View.VISIBLE);
            this.addImageButton.setVisibility(View.GONE);
        } else if (this.imageUrl != null && this.imageUri == null) {
            Glide.with(this).load(imageUrl).centerCrop().into(this.attachedImage);
            this.attachedImage.setVisibility(View.VISIBLE);
            this.layoutImageEditor.setVisibility(View.VISIBLE);
            this.addImageButton.setVisibility(View.GONE);
        } else {
            this.attachedImage.setImageURI(null);
            this.attachedImage.setVisibility(View.INVISIBLE);
            this.addImageButton.setVisibility(View.VISIBLE);
            this.layoutImageEditor.setVisibility(View.GONE);
        }
    }
}
